from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EjAnalysisConfig(AppConfig):
    name = "ej_analysis"
    verbose_name = _("Analysis")
